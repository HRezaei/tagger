Ext.define('Tagger.view.Single', {
    extend: 'Ext.panel.Panel',
    xtype: 'single',
    requires:[
      'Ext.layout.container.Border',
    	'Tagger.view.ElementEditor',
      'Tagger.view.TagDefiner',
      'Ext.form.Label',
    	'Ext.form.field.HtmlEditor'
    ],
    layout: 'border',
    closable: true,
    items: [{
        region: 'west',
        title: 'west',
        layout: 'fit',
        flex: 3,
        split: true,
        dockedItems: {
          xtype: 'label',
          itemId: 'selectedNodeText',
          text: ''
        },
        items: {
        	xtype: 'elementeditor'
        },
        width: 150
    },{
        region: 'center',
        flex: 7,
        xtype: 'htmleditor',
        rtl: true,
        enableColors: false,
        enableFont: false,
        enableFontSize: false,
        enableFormat: false,
        enableAlignments: false,
        enableLinks: false,
        enableLists: false,
        title: 'Center Tab 1',
        listeners: {
            activate: function() {
                var me = this;
                this.getWin().document.onmouseup =  function(e){
                    var selection = this.getSelection(),
                        selectedText = selection.toString(),
                        panel = me.up('single'),
                        node = false;
                    if(selectedText) {
                      if(selection.focusNode.parentNode.textContent == selectedText) {
                        var NodeType = selection.focusNode.parentNode.localName;
                        node = selection.focusNode.parentNode;
                      }
                      else {
                        node = selection.focusNode;
                      }
                    }
                    panel.setNode(node);
                };
            },
            editmodechange: function(editor, sourceEdit, eOpts ) {
              if(!sourceEdit){
                return;
              }               
              xml_raw = editor.getValue();
              xml_formatted = vkbeautify.xml(xml_raw);              
              editor.setValue(xml_formatted);
            }
        },
        value: "به نام خدای بخشنده و \n \
        <esm>ali</esm>\
<sefat type='بیانی'>\
مهربان\
</sefat>\
، ای . نام تو بهترین aliسرآغاز"
    }],
    getSelection: function(){
      var editor = this.down('htmleditor'),
        selection = editor.getWin().document.getSelection();
      return selection;
    },
    setNode: function(node) {
      var elementEditor = this.down('elementeditor'),
        mainLabel = this.down('#selectedNodeText'),
        text = node ? node.textContent : '';
      elementEditor.setElement(node);
      mainLabel.setText(text);
    }
});