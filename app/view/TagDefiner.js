Ext.define('Tagger.view.TagDefiner', {
	extend: 'Ext.form.Panel',
	title: 'Define new Tag',
	floating: true,
	closable: true,
	bodyPadding: 10,
	items: [{
		xtype: 'textfield',
		name: 'humanReadableTag',
		fieldLabel: 'Tag name',
	}, {
		xtype: 'textfield',
		name: 'mashinReadableTag',
		fieldLabel: 'Tag name in code',
	}, {
		xtype: 'button',
		text: 'Save',
		handler: function() {
			var panel = this.up('form'),
				form = panel.getForm(),
				values = form.getValues();
			panel.saveHandler(values.mashinReadableTag, values.humanReadableTag);
		}
	}] 
});