/**
In the name of GOD.
@todo: Add the feature to remove properties

*/
Ext.define('Tagger.view.property.PropGrid', {
    extend: 'Ext.grid.PropertyGrid',
    alias: 'widget.propgrid',
    initComponent : function(){
        var me = this;
        this.callParent();
        // Enable cell editing. Inject a custom startEdit which always edits column 1 regardless of which column was clicked.
        //me.plugins.push(new Ext.grid.plugin.CellEditing({
        me.plugins[0].startEdit = function(record, column) {
            // Maintainer: Do not change this 'this' to 'me'! It is the CellEditing object's own scope.
            return this.self.prototype.startEdit.call(this, record, me.headerCt.child('#' + column.dataIndex));
        };

        var getPropertyNameEditor = function(record){
            return me.getPropertyNameEditor(record, this);
        };
        me.columns[0].getEditor = Ext.Function.bind(getPropertyNameEditor, me.columns);

        me.addEvents(
            'newpropertyadded',
            'beforepropertynamechange',
            'propertynamechange'
        );
    },

    // @private
    onUpdate : function(store, record, operation) {
        var me = this,
            v, oldValue;

        if (me.rendered && operation == Ext.data.Model.EDIT) {
            if('name' in record.modified) {
                var name = record.get(me.nameField);
                var oldName = record.modified.name;
                if(oldName == 'Add property...') {
                    me.fireEvent('newpropertyadded', me.source, record.getId(), name);
                    record.commit();
                    me.setProperty('Add property...', '', true);
                    return;
                }
                if (me.fireEvent('beforepropertynamechange', me.source, record.getId(), name, oldName) !== false) {
                    if (me.source) {
                        var value = me.source[oldName];
                        delete me.source[oldName];
                        me.source[name] = value;
                    }
                    record.commit();
                    me.fireEvent('propertynamechange', me.source, record.getId(), name, oldName);
                } else {
                    record.reject();
                }
            }
            else if('value' in record.modified) {
                v = record.get(me.valueField);
                oldValue = record.modified.value;
                if (me.fireEvent('beforepropertychange', me.source, record.getId(), v, oldValue) !== false) {
                    if (me.source) {
                        me.source[record.getId()] = v;
                    }
                    record.commit();
                    me.fireEvent('propertychange', me.source, record.getId(), v, oldValue);
                } else {
                    record.reject();
                }
            }
            else {
                Ext.Error.raise('Not implemented yet!');
            }
        }
    },
    getPropertyNameEditor : function(record, column) {
        var me = this,
            editors = me.editors;
        return editors.string;
    },
    setSource: function(source, sourceConfig) {
        //@todo: check if the source already has a 'Add property...' name
        //if(source.)
        source['Add property...'] = '';
        this.callParent(arguments);
    }
});
