Ext.define('Tagger.view.ElementEditor', {
	extend: 'Ext.panel.Panel',
	xtype: 'elementeditor',
	requires: [
		'Tagger.view.property.PropGrid',
		'Ext.form.field.ComboBox'
	],
	dockedItems: [ {
    xtype: 'combo',
    queryMode: 'local',
    valueField: 'tagName',
    displayField: 'tagTitle',
    store: {
        fields: [
            'tagName',  // numeric value is the key
            'tagTitle'
        ],
        data: [
          {tagName: 'newTag', tagTitle:'Add new tag...'}
        ]
    },
    listeners: {
      select: function( combo, records, eOpts){
        if(records.length == 1 && records[0].get('tagName') == 'newTag') {
          var win = Ext.create('Tagger.view.TagDefiner', {
            saveHandler: function(name, text) {
              var inserted = combo.store.insert(0, {tagName: name, tagTitle: text});
              combo.select();
              combo.expand();
              win.close();
            }
          });
          win.show();
        }
      },
      change: function(combo, newValue, oldValue, eOpts ) {
        var editor = this.up('elementeditor')
          currentElement = editor.currentElement;;
            if(!currentElement) {
              return;
            }
            //Here, the decision of replace/insert, has been done by checking oldValue
            if(oldValue) {
              //nodeType must be replaced
              var parent = Ext.get(currentElement.parentNode);
              var current = Ext.get(currentElement);
              var newNode = Ext.DomHelper.createDom({tag: newValue, html:currentElement.innerHTML});
              //this.currentElement.parentNode.replaceChild(newNode , this.currentElement);
              /* or*/
                current.replaceWith(newNode);
              /*  or
                newNode.replace(current);
              */
            }
            else {
              //must insert new node
              var selection = this.up('single').getSelection()
                range = selection.getRangeAt(0),
                inner = range.toString();
              range.deleteContents();
              var n = document.createElement(newValue);
              n.innerText = inner;  
              range.insertNode(n);
            }
      }
    }
	}],
	items:[{
  	xtype: 'propgrid',
    sortableColumns: false,
    listeners: {
        propertychange: function(source, id, value, oldValue){
            var elementEditor = this.up('elementeditor');
            if(!elementEditor.currentElement) {
              return;
            }
            elementEditor.currentElement.setAttribute(id, value);
        },
        propertynamechange: function(source, id, name, oldName) {
          var elementEditor = this.up('elementeditor');
          if(!elementEditor.currentElement) {
            return;
          }
          var node = Ext.get(elementEditor.currentElement);
          if(node) {
              var attrValue = node.dom.getAttribute(oldName);
              //delete this.currentElement.attributes[oldName];
              //node.dom.setAttribute(oldName, null);
              node.dom.removeAttribute(oldName)
              node.dom.setAttribute(name, attrValue);
          }
        }
    }
  }],
  setElement: function(element) {
  	var propGrid = this.down('propgrid'),
      combo = this.down('combo');
    if(!element) {
      propGrid.setSource(false);
      this.currentElement = false;
      combo.clearValue();
      return;
    }
    var nodeType = element.localName,
      attributes = [];
    combo.select(nodeType, true);
    if (element.attributes) {
      var attrs = element.attributes;
      for(var i = 0; i < attrs.length; i++) {
          var attr = attrs[i];
          attributes[attr.name] = attr.value;
      }
    }
    propGrid.setSource(attributes);
    this.currentElement = element;
  }
});