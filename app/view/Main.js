Ext.define('Tagger.view.Main', {
    extend: 'Ext.panel.Panel',
    requires:[
        'Ext.tab.Panel',
        'Tagger.view.Single'
    ],
    
    xtype: 'app-main',

    layout: 'fit',
    dockedItems: [{
        xtype: 'toolbar',
        items:[{
            xtype: 'tool',
            type: 'plus',
            handler: function(){
                this.up('app-main').down('tabpanel').add({
                    xtype: 'single',
                    title: 'new document'
                });
            }
        }]
    }],
    items: [{
        xtype: 'tabpanel',
        items:[{
            xtype: 'single',
            title: 'hi'
        }]
    }]
});